/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 12:06:22 by ameur             #+#    #+#             */
/*   Updated: 2021/03/25 23:40:45 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

t_lst	*ft_lstnew(void *content)
{
	t_lst *new_list;

	if (!(new_list = malloc(sizeof(t_lst))))
		return (NULL);
	new_list->content = content;
	new_list->next = NULL;
	return (new_list);
}

int		ft_lstsize(t_lst *lst)
{
	int		i;
	t_lst	*tmp;

	i = 1;
	if (!(lst))
		return (0);
	tmp = lst->next;
	while (tmp != NULL)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

void	del(char *content)
{
	free(content);
	content = NULL;
}

void	ft_lstclear(t_struct *cfg)
{
	t_lst *tmp;

	if (!(cfg->lst))
		return ;
	if ((cfg->lst) == NULL)
		return ;
	while (cfg->lst != NULL)
	{
		tmp = cfg->lst->next;
		free(cfg->lst->content);
		free(cfg->lst);
		cfg->lst = tmp;
	}
}

void	ft_lstdelone(t_lst *lst, void (*del)(void*))
{
	if (del && lst && lst->content)
	{
		del(lst->content);
		free(lst);
	}
}
