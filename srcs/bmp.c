/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 19:00:05 by ameur             #+#    #+#             */
/*   Updated: 2021/03/26 22:41:40 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_str_cmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i])
	{
		if (s1[i] != s2[i])
			return (1);
		i++;
	}
	return (0);
}

void	ft_header_bmp(t_struct *cfg, int fd)
{
	int tmp;

	write(fd, "BM", 2);
	tmp = 4 * cfg->rx * cfg->ry;
	write(fd, &tmp, 4);
	tmp = 0;
	write(fd, &tmp, 4);
	tmp = 54;
	write(fd, &tmp, 4);
	tmp = 40;
	write(fd, &tmp, 4);
	write(fd, &cfg->rx, 4);
	write(fd, &cfg->ry, 4);
	tmp = 1;
	write(fd, &tmp, 2);
	write(fd, &cfg->img.bits_per_pixel, 2);
	tmp = 0;
	write(fd, &tmp, 4);
	write(fd, &tmp, 4);
	write(fd, &tmp, 4);
	write(fd, &tmp, 4);
	write(fd, &tmp, 4);
	write(fd, &tmp, 4);
}

void	ft_save_bmp(t_struct *cfg)
{
	int fd;
	int i;
	int j;

	fd = open("./Cub3D.bmp", O_CREAT | O_RDWR);
	i = cfg->ry;
	ft_header_bmp(cfg, fd);
	while (i >= 0)
	{
		j = 0;
		while (j < cfg->rx)
		{
			write(fd, &cfg->img.addr[i * cfg->img.l_l / 4 + j], 4);
			j++;
		}
		i--;
	}
	ft_exit(cfg);
}
