/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/19 01:28:35 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/21 14:46:08 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

/*
** calcule ray position and direction
** calcule lenght of ray from x or y to next x or y (delta_dist_x et y)
*/

void	ft_calcul(t_struct *cfg, t_ray *ray, int x)
{
	ray->camera_x = 2 * x / (double)cfg->rx - 1;
	ray->ray_dir_x = cfg->player.dir_x + cfg->player.plane_x *
		ray->camera_x;
	ray->ray_dir_y = cfg->player.dir_y + cfg->player.plane_y *
		ray->camera_x;
	ray->map_x = (int)cfg->player.x;
	ray->map_y = (int)cfg->player.y;
	ray->delta_dist_x = fabs(1 / ray->ray_dir_x);
	ray->delta_dist_y = fabs(1 / ray->ray_dir_y);
	ray->hit = 0;
}

/*
**calcule step and inital side_dist
*/

void	ft_side_dist(t_struct *cfg, t_ray *ray)
{
	if (ray->ray_dir_x < 0)
	{
		ray->step_x = -1;
		ray->side_dist_x = (cfg->player.x - ray->map_x) * ray->delta_dist_x;
	}
	else
	{
		ray->step_x = 1;
		ray->side_dist_x = (ray->map_x + 1.0 - cfg->player.x) *
			ray->delta_dist_x;
	}
	if (ray->ray_dir_y < 0)
	{
		ray->step_y = -1;
		ray->side_dist_y = (cfg->player.y - ray->map_y) * ray->delta_dist_y;
	}
	else
	{
		ray->step_y = 1;
		ray->side_dist_y = (ray->map_y + 1.0 - cfg->player.y) *
			ray->delta_dist_y;
	}
}

/*
** jump to next map square (x or y) and check if ray has hit a wall
** caclulate distance of perpendicular ray (perp_wall_dist)
*/

void	ft_dda(t_struct *cfg, t_ray *ray)
{
	while (ray->hit == 0)
	{
		if (ray->side_dist_x < ray->side_dist_y)
		{
			ray->side_dist_x += ray->delta_dist_x;
			ray->map_x += ray->step_x;
			ray->side = 0;
		}
		else
		{
			ray->side_dist_y += ray->delta_dist_y;
			ray->map_y += ray->step_y;
			ray->side = 1;
		}
		if (cfg->map[ray->map_x][ray->map_y] == '1')
			ray->hit = 1;
	}
	if (ray->side == 0)
		ray->perp_wall_dist = (ray->map_x - cfg->player.x +
			(1 - ray->step_x) / 2) / ray->ray_dir_x;
	else
		ray->perp_wall_dist = (ray->map_y - cfg->player.y +
			(1 - ray->step_y) / 2) / ray->ray_dir_y;
}

void	ft_line_height(t_struct *cfg, t_ray *ray)
{
	ray->line_height = (int)(cfg->ry / ray->perp_wall_dist);
	ray->draw_start = -ray->line_height / 2 + cfg->ry / 2;
	if (ray->draw_start <= 0)
		ray->draw_start = 0;
	ray->draw_end = ray->line_height / 2 + cfg->ry / 2;
	if (ray->draw_end >= cfg->ry)
		ray->draw_end = cfg->ry;
	if (ray->side == 0)
		ray->wall_x = cfg->player.y + ray->perp_wall_dist * ray->ray_dir_y;
	else
		ray->wall_x = cfg->player.x + ray->perp_wall_dist * ray->ray_dir_x;
	ray->wall_x -= floor((ray->wall_x));
}

int		ft_raycast(t_struct *cfg)
{
	int		x;
	t_ray	ray;

	x = 0;
	if (!(cfg->z_buffer = malloc(cfg->rx * sizeof(double))))
		return (1);
	while (x < cfg->rx)
	{
		ft_calcul(cfg, &ray, x);
		ft_side_dist(cfg, &ray);
		ft_dda(cfg, &ray);
		ft_line_height(cfg, &ray);
		if (ray.map_x < cfg->player.x && ray.side == 0)
			draw_line(cfg, &ray, x, 0);
		else if (ray.side == 0)
			draw_line(cfg, &ray, x, 1);
		else if (ray.map_y < cfg->player.y && ray.side == 1)
			draw_line(cfg, &ray, x, 2);
		else if (ray.side == 1)
			draw_line(cfg, &ray, x, 3);
		cfg->z_buffer[x] = ray.perp_wall_dist;
		x++;
	}
	ft_sprites(cfg);
	return (0);
}
