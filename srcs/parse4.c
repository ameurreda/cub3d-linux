/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 23:35:03 by ameur             #+#    #+#             */
/*   Updated: 2021/04/19 17:24:00 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_is_v(char c, int v)
{
	if (c >= 48 && c <= 57)
		return (1);
	if (c == 32)
		return (1);
	if (c == ',' && v < 2)
		return (1);
	if (c == 0)
		return (1);
	return (0);
}

int		ft_parse_c_t(t_struct *cfg, char *line, int i)
{
	if (cfg->c.r == -1)
	{
		cfg->c.r = ft_atoi(&line[i]);
		i = ft_count_i(line, i);
	}
	else if (cfg->c.g == -1)
	{
		cfg->c.g = ft_atoi(&line[i]);
		i = ft_count_i(line, i);
	}
	else if (cfg->c.b == -1)
	{
		cfg->c.b = ft_atoi(&line[i]);
		i = ft_count_i(line, i);
	}
	else
		return (1);
	return (0);
}

int		ft_is_v_map(char c)
{
	if (c == '1' || c == '0' || c == '2' ||
		c == 'N' || c == 'S' || c == 'E' ||
		c == 'W' || c == ' ')
		return (1);
	return (0);
}
