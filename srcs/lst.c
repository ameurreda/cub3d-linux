/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/12 04:02:53 by ameurreda         #+#    #+#             */
/*   Updated: 2021/03/25 23:40:35 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	ft_lstiter(t_lst *lst, int (printf)(const char *, ...))
{
	while (lst != NULL)
	{
		printf("lst = %s\n", lst->content);
		lst = lst->next;
	}
}

t_lst	*ft_lstlast(t_lst *lst)
{
	t_lst	*tmp;

	if (!(lst))
		return (NULL);
	tmp = lst;
	while (tmp->next != NULL)
		tmp = tmp->next;
	return (tmp);
}

void	ft_lstadd_back(t_lst **alst, t_lst *new)
{
	t_lst *tmp;

	if (alst == NULL)
		return ;
	if (*alst == NULL)
	{
		(*alst) = new;
		new->next = NULL;
		return ;
	}
	tmp = ft_lstlast(*alst);
	tmp->next = new;
	new->next = NULL;
}

t_lst	*ft_lstmap(t_lst *lst, void *(*f)(void *), void (*del)(void *))
{
	t_lst *res;
	t_lst *start;

	if (!lst || !f)
		return (NULL);
	res = ft_lstnew((f)(lst->content));
	start = res;
	while (lst->next != NULL)
	{
		lst = lst->next;
		if (!(res->next = ft_lstnew((f)(lst->content))))
		{
			ft_lstdelone((res->next), del);
			return (NULL);
		}
		res = res->next;
	}
	return (start);
}
