/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/14 19:32:51 by ameurreda         #+#    #+#             */
/*   Updated: 2021/03/26 21:00:53 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int	is_player(t_struct *cfg, int i, int j)
{
	if (cfg->map[i][j] == 'N' || cfg->map[i][j] == 'S' ||
		cfg->map[i][j] == 'E' || cfg->map[i][j] == 'W')
		return (1);
	return (0);
}

int	rgb_to_int(int r, int g, int b)
{
	int	result;

	result = 0;
	result += r << 16;
	result += g << 8;
	result += b;
	return (result);
}

int	ft_parse_line_t(char *line, t_struct *cfg, int i)
{
	if (line[i] == 'R')
	{
		if (cfg->rx != -1 || cfg->ry != -1 || ft_parse_r(line, cfg, i + 1) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	}
	else if (line[i] == 'N' && line[i + 1] == 'O')
	{
		if (cfg->tex[0].path != NULL || ft_parse_path(line, cfg, 1, i + 2) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	}
	else if (line[i] == 'W' && line[i + 1] == 'E')
		if (cfg->tex[2].path != NULL || ft_parse_path(line, cfg, 3, i + 2) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	if (ft_is_info(line, cfg, i) == 0)
		return (1);
	return (0);
}
