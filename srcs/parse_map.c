/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/12 04:29:28 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/20 14:43:44 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_len(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int		ft_longest_line(t_lst *lst)
{
	int i;
	int res;

	res = 0;
	while (lst != NULL)
	{
		i = ft_len(lst->content);
		if (i > res)
			res = i;
		lst = lst->next;
	}
	return (res);
}

int		ft_is_close(int x, int y, t_struct *cfg, char **map)
{
	if (x < 0 || x >= cfg->size_map || y < 0 || y >= ft_len(map[x]))
		return (-1);
	if (map[x][y] == '1')
		return (1);
	if (map[x][y] != '0' && map[x][y] != '2')
		return (-1);
	map[x][y] = '1';
	if (ft_is_close(x + 1, y, cfg, map) == 1 &&
			ft_is_close(x, y + 1, cfg, map) == 1 &&
				ft_is_close(x - 1, y, cfg, map) == 1 &&
					ft_is_close(x, y - 1, cfg, map) == 1)
		return (1);
	return (0);
}

char	**clone_map(t_struct *cfg)
{
	char	**map;
	int		i;

	i = 0;
	if (!(map = malloc(sizeof(char*) * cfg->size_map + 1)))
		return (NULL);
	while (i < cfg->size_map)
	{
		if (!(map[i] = malloc(sizeof(char) * (ft_len(cfg->map[i]) + 1))))
			return (NULL);
		ft_cpy(cfg->map[i], map[i], 0);
		i++;
	}
	return (map);
}

int		ft_check_map(t_struct *cfg)
{
	int		x;
	int		y;
	int		i;
	char	**map;

	map = clone_map(cfg);
	i = 0;
	x = cfg->player.x;
	y = cfg->player.y;
	if (ft_is_close(x, y, cfg, map) != 1)
	{
		while (i < cfg->size_map)
			free(map[i++]);
		free(map);
		return (1);
	}
	while (i < cfg->size_map)
		free(map[i++]);
	free(map);
	return (0);
}
