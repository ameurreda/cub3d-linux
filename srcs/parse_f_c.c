/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_f_c.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 15:52:49 by ameur             #+#    #+#             */
/*   Updated: 2021/03/25 23:41:00 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_parse_f_t(t_struct *cfg, char *line, int i)
{
	if (cfg->f.r == -1)
	{
		cfg->f.r = ft_atoi(&line[i]);
		i = ft_count_i(line, i);
	}
	else if (cfg->f.g == -1)
	{
		cfg->f.g = ft_atoi(&line[i]);
		i = ft_count_i(line, i);
	}
	else if (cfg->f.b == -1)
	{
		cfg->f.b = ft_atoi(&line[i]);
		i = ft_count_i(line, i);
	}
	else
		return (1);
	return (0);
}

int		ft_parse_f_bis(t_struct *cfg, char *line, int i)
{
	int	v;

	v = 0;
	while (line[i])
	{
		if (line[i] >= 48 && line[i] <= 57)
		{
			if (ft_parse_f_t(cfg, line, i) == 1)
				return (1);
			i = ft_count_i(line, i);
		}
		if (ft_is_v(line[i], v) == 0)
			return (1);
		if (line[i] == ',')
			v++;
		if (line[i] != '\0')
			i++;
	}
	return (0);
}

int		ft_parse_f(char *line, t_struct *cfg, int i)
{
	if (ft_parse_f_bis(cfg, line, i) == 1)
		return (1);
	if (ft_check_rgb(cfg, 1) == 1)
	{
		cfg->res = 3;
		return (1);
	}
	return (0);
}

int		ft_parse_c_bis(t_struct *cfg, char *line, int i)
{
	int	v;

	v = 0;
	while (line[i])
	{
		if (line[i] >= 48 && line[i] <= 57)
		{
			if (ft_parse_c_t(cfg, line, i) == 1)
				return (1);
			i = ft_count_i(line, i);
		}
		if (ft_is_v(line[i], v) == 0)
			return (1);
		if (line[i] == ',')
			v++;
		if (line[i] != '\0')
			i++;
	}
	return (0);
}

int		ft_parse_c(char *line, t_struct *cfg, int i)
{
	if (ft_parse_c_bis(cfg, line, i) == 1)
		return (1);
	if (ft_check_rgb(cfg, 2) == 1)
	{
		cfg->res = 4;
		return (1);
	}
	return (0);
}
