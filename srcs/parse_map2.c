/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 12:50:02 by ameur             #+#    #+#             */
/*   Updated: 2021/04/20 14:43:27 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

char	*ft_dup(char *s)
{
	int		i;
	char	*res;

	i = ft_len(s);
	if (!(res = malloc(i * sizeof(char) + 1)))
		return (NULL);
	res[i] = '\0';
	i--;
	while (i >= 0)
	{
		res[i] = s[i];
		i--;
	}
	return (res);
}

void	ft_cpy(char *src, char *dest, int i)
{
	int j;

	j = 0;
	while (src[i])
	{
		dest[j] = src[i];
		i++;
		j++;
	}
	dest[j] = '\0';
}

int		ft_lst_to_map(t_struct *cfg)
{
	int		i;
	int		lg_line;
	t_lst	*tmp;

	tmp = cfg->lst;
	lg_line = ft_longest_line(tmp);
	cfg->size_map = ft_lstsize(tmp);
	i = 0;
	if (!(cfg->map = malloc(sizeof(char*) * cfg->size_map + 1)))
		return (1);
	while (i < cfg->size_map)
	{
		if (!(cfg->map[i] = malloc(sizeof(char) * (lg_line + 1))))
		{
			cfg->size_map = i + 1;
			return (1);
		}
		ft_cpy(tmp->content, cfg->map[i], 0);
		tmp = tmp->next;
		i++;
	}
	return (0);
}

int		ft_check_line(t_struct *cfg, int i)
{
	int	j;

	if (ft_len(cfg->map[i]) > ft_len(cfg->map[i - 1]))
	{
		j = ft_len(cfg->map[i - 1]) - 1;
		while (++j < (ft_len(cfg->map[i]) - 1))
			if (cfg->map[i][j] != '1')
				return (1);
	}
	if (ft_len(cfg->map[i]) > ft_len(cfg->map[i + 1]))
	{
		j = ft_len(cfg->map[i + 1]) - 1;
		while (++j < (ft_len(cfg->map[i]) - 1))
		{
			if (cfg->map[i][j] != '1')
				return (1);
		}
	}
	if (cfg->map[i][0] != '1')
		return (1);
	if (cfg->map[i][ft_len(cfg->map[i]) - 1] != '1')
		return (1);
	return (0);
}

int		ft_is_cub(char **argv)
{
	int		i;
	char	*s;

	s = argv[1];
	i = ft_len(s) - 1;
	if (s[i] == 'b' && s[i - 1] == 'u' && s[i - 2] == 'c'
				&& s[i - 3] == '.')
		return (1);
	return (0);
}
