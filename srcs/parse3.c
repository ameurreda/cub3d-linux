/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 23:29:55 by ameur             #+#    #+#             */
/*   Updated: 2021/04/15 08:10:33 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_is_info(char *s, t_struct *cfg, int i)
{
	if (s[i] == 'F' || s[i] == 'C' || s[i] == 'R' ||
		s[i] == '1' || s[i] == ' ' || s[i] == '\n' ||
		s[i] == '\0')
		return (1);
	if ((s[i] == 'N' && s[i + 1] == 'O') || (s[i] == 'S'
			&& s[i + 1] == 'O') || (s[i] == 'W' && s[i + 1] == 'E')
			|| (s[i] == 'E' && s[i + 1] == 'A'))
		return (1);
	if (s[i] == 'S' && cfg->tex[4].path == NULL)
		return (1);
	cfg->res = 9;
	return (0);
}

int		ft_is_valid(char c, t_struct *cfg)
{
	if ((c >= 48 && c <= 57) || c == ' ')
	{
		if (c == ' ')
			return (1);
		if (cfg->rx != -1 && cfg->ry != -1)
			return (0);
		return (1);
	}
	return (0);
}

void	ft_init_struct(t_struct *cfg)
{
	cfg->rx = -1;
	cfg->ry = -1;
	cfg->tex[0].path = NULL;
	cfg->tex[1].path = NULL;
	cfg->tex[2].path = NULL;
	cfg->tex[3].path = NULL;
	cfg->tex[4].path = NULL;
	cfg->tex[0].tex = NULL;
	cfg->tex[1].tex = NULL;
	cfg->tex[2].tex = NULL;
	cfg->tex[3].tex = NULL;
	cfg->tex[4].tex = NULL;
	cfg->map = NULL;
	cfg->lst = NULL;
	cfg->f.r = -1;
	cfg->f.g = -1;
	cfg->f.b = -1;
	cfg->c.r = -1;
	cfg->c.g = -1;
	cfg->c.b = -1;
	cfg->res = 0;
	cfg->save = -1;
	cfg->is_map = 0;
}

int		ft_line_to_lst(t_struct *cfg, char *line)
{
	char *str;

	str = NULL;
	if (ft_check_path(cfg) == 1 || ft_check_rgb(cfg, 1) == 1 ||
			ft_check_rgb(cfg, 2) == 1 || cfg->rx == -1 || cfg->ry == -1)
	{
		cfg->res = 10;
		free(line);
		return (1);
	}
	cfg->is_map = 1;
	str = ft_dup(line);
	if (str == NULL)
	{
		free(line);
		cfg->res = 17;
		return (1);
	}
	ft_lstadd_back(&cfg->lst, ft_lstnew(str));
	return (0);
}

int		ft_count_i(char *line, int i)
{
	while (line[i] >= 48 && line[i] <= 57)
		i++;
	return (i);
}
