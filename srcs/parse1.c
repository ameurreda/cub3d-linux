/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/20 12:27:29 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/19 17:24:20 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_parse_line_bis(char *line, t_struct *cfg, int i)
{
	if (ft_parse_line_t(line, cfg, i) == 1)
		return (1);
	else if (line[i] == 'S' && line[i + 1] == 'O')
	{
		if (cfg->tex[1].path != NULL || ft_parse_path(line, cfg, 2, i + 2) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	}
	else if (line[i] == 'S')
	{
		if (cfg->tex[4].path != NULL || ft_parse_path(line, cfg, 5, i + 1) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	}
	else if (line[i] == 'E' && line[i + 1] == 'A')
		if (cfg->tex[3].path != NULL || ft_parse_path(line, cfg, 4, i + 2) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	return (0);
}

int		ft_parse_f_c(char *line, t_struct *cfg, int i)
{
	if (line[i] == 'F')
	{
		if (cfg->f.r != -1 || ft_parse_f(line, cfg, i + 1) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	}
	else if (line[i] == 'C')
		if (cfg->c.r != -1 || ft_parse_c(line, cfg, i + 1) == 1)
		{
			cfg->res = 8;
			return (1);
		}
	return (0);
}

int		ft_parse_line(char *line, t_struct *cfg)
{
	int i;

	i = 0;
	while (line[i] == ' ')
		i++;
	if (line[i] == '1' && ft_check_parse(cfg) == 1)
	{
		cfg->res = 11;
		return (1);
	}
	if ((line[i] != '1' && cfg->is_map == 1))
	{
		cfg->res = 15;
		return (1);
	}
	if (ft_parse_line_bis(line, cfg, i) == 1)
		return (1);
	if (ft_parse_f_c(line, cfg, i) == 1)
		return (1);
	return (0);
}

int		ft_parse_bis(t_struct *cfg)
{
	if (ft_check_parse(cfg) == 1)
		return (1);
	if (cfg->is_map == 0 || ft_lst_to_map(cfg) == 1)
	{
		cfg->res = 7;
		return (1);
	}
	if (ft_parse_player(cfg) == 1 || ft_check_map(cfg) == 1)
	{
		cfg->res = 13;
		return (1);
	}
	cfg->nb_sprite = ft_count_nb_spr(cfg);
	if (ft_parse_sprite(cfg) == 1)
	{
		cfg->res = 16;
		return (1);
	}
	return (0);
}

int		ft_parse(int fd, t_struct *cfg)
{
	char		*line;
	int			ret;

	ret = 1;
	while (ret > 0)
	{
		ret = get_next_line(fd, &line);
		if (ft_parse_line(line, cfg) == 1)
		{
			free(line);
			return (1);
		}
		else if (ft_is_map(line) == 0)
		{
			if (ft_line_to_lst(cfg, line) == 1)
				return (1);
		}
		free(line);
	}
	if (ft_parse_bis(cfg) == 1)
		return (1);
	return (0);
}
