/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/24 22:22:33 by ameur             #+#    #+#             */
/*   Updated: 2021/04/20 14:26:12 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	ft_free_map(t_struct *cfg)
{
	int i;

	i = 0;
	while (i < cfg->size_map)
	{
		free(cfg->map[i]);
		i++;
	}
	free(cfg->map);
}

void	ft_exit_bis(t_struct *cfg)
{
	close(cfg->fd);
	if (cfg->res == 6)
	{
		free(cfg->sprites);
		mlx_destroy_image(cfg->img.mlx, cfg->img.img);
		mlx_destroy_display(cfg->img.mlx);
		free(cfg->img.mlx);
	}
	if (cfg->res == 0)
	{
		free(cfg->sprites);
		mlx_destroy_image(cfg->img.mlx, cfg->img.img);
		if (cfg->save == -1)
			mlx_destroy_window(cfg->img.mlx, cfg->img.mlx_win);
		mlx_destroy_display(cfg->img.mlx);
		free(cfg->img.mlx);
	}
	exit(0);
}

int		ft_exit(t_struct *cfg)
{
	if (cfg->tex[0].path != NULL)
		free(cfg->tex[0].path);
	if (cfg->tex[0].tex != NULL)
		mlx_destroy_image(cfg->img.mlx, cfg->tex[0].tex);
	if (cfg->tex[1].path != NULL)
		free(cfg->tex[1].path);
	if (cfg->tex[1].tex != NULL)
		mlx_destroy_image(cfg->img.mlx, cfg->tex[1].tex);
	if (cfg->tex[2].path != NULL)
		free(cfg->tex[2].path);
	if (cfg->tex[2].tex != NULL)
		mlx_destroy_image(cfg->img.mlx, cfg->tex[2].tex);
	if (cfg->tex[3].path != NULL)
		free(cfg->tex[3].path);
	if (cfg->tex[3].tex != NULL)
		mlx_destroy_image(cfg->img.mlx, cfg->tex[3].tex);
	if (cfg->tex[4].path != NULL)
		free(cfg->tex[4].path);
	if (cfg->tex[4].tex != NULL)
		mlx_destroy_image(cfg->img.mlx, cfg->tex[4].tex);
	if (cfg->map != NULL)
		ft_free_map(cfg);
	ft_lstclear(cfg);
	ft_exit_bis(cfg);
	return (0);
}

void	ft_print_error(t_struct *cfg)
{
	printf("Error.\n");
	if (cfg->res == 2)
		printf("Resolution is not valid.\n");
	else if (cfg->res == 1)
		printf("Malloc for the path failed.\n");
	else if (cfg->res == 3)
		printf("RGB for the floor isn't valid.\n");
	else if (cfg->res == 4)
		printf("RGB for the ceiling isn't valid.\n");
	else if (cfg->res == 7 || cfg->res == 11)
		printf("Map is not valid.\n");
	else if (cfg->res == 13)
		printf("Map is not valid.\n");
	else if (cfg->res == 5)
		printf("Missing texture path.\n");
	else if (cfg->res == 6)
		printf("The texture path is unvalid.\n");
	else
		printf("The configuration file is unvalid.\n");
}
