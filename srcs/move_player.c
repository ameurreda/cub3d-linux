/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_player.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/08 04:52:09 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/21 14:54:45 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	ft_step_front_back(t_struct *cfg, int keycode)
{
	if (keycode == 119)
	{
		if (cfg->map[(int)(cfg->player.x + cfg->player.dir_x * 0.1)]
				[(int)cfg->player.y] == '0')
			cfg->player.x += cfg->player.dir_x * 0.1;
		if (cfg->map[(int)cfg->player.x]
			[(int)(cfg->player.y + cfg->player.dir_y * 0.1)] == '0')
			cfg->player.y += cfg->player.dir_y * 0.1;
	}
	if (keycode == 115)
	{
		if (cfg->map[(int)(cfg->player.x - cfg->player.dir_x * 0.1)]
					[(int)cfg->player.y] == '0')
			cfg->player.x -= cfg->player.dir_x * 0.1;
		if (cfg->map[(int)cfg->player.x]
				[(int)(cfg->player.y - cfg->player.dir_y * 0.1)] == '0')
			cfg->player.y -= cfg->player.dir_y * 0.1;
	}
}

void	ft_step_right_left(t_struct *cfg, int keycode)
{
	if (keycode == 100)
	{
		if (cfg->map[(int)(cfg->player.x + cfg->player.plane_x * 0.1)]
				[(int)cfg->player.y] == '0')
			cfg->player.x += cfg->player.plane_x * 0.1;
		if (cfg->map[(int)cfg->player.x]
			[(int)(cfg->player.y + cfg->player.plane_y * 0.1)] == '0')
			cfg->player.y += cfg->player.plane_y * 0.1;
	}
	if (keycode == 97)
	{
		if (cfg->map[(int)(cfg->player.x - cfg->player.plane_x * 0.1)]
				[(int)cfg->player.y] == '0')
			cfg->player.x -= cfg->player.plane_x * 0.1;
		if (cfg->map[(int)cfg->player.x]
			[(int)(cfg->player.y - cfg->player.plane_y * 0.1)] == '0')
			cfg->player.y -= cfg->player.plane_y * 0.1;
	}
}

void	ft_look_left(t_struct *cfg)
{
	double		old_dir_x;
	double		old_plane_x;

	old_dir_x = cfg->player.dir_x;
	cfg->player.dir_x = cfg->player.dir_x * cos((0.033 * 1.8) / 2) -
						cfg->player.dir_y * sin((0.033 * 1.8) / 2);
	cfg->player.dir_y = old_dir_x * sin((0.033 * 1.8) / 2) +
						cfg->player.dir_y * cos((0.033 * 1.8) / 2);
	old_plane_x = cfg->player.plane_x;
	cfg->player.plane_x = cfg->player.plane_x * cos((0.033 * 1.8) / 2)
						- cfg->player.plane_y * sin((0.033 * 1.8) / 2);
	cfg->player.plane_y = old_plane_x * sin((0.033 * 1.8) / 2) +
						cfg->player.plane_y * cos((0.033 * 1.8) / 2);
}

void	ft_look_right(t_struct *cfg)
{
	double		old_dir_x;
	double		old_plane_x;

	old_dir_x = cfg->player.dir_x;
	cfg->player.dir_x = cfg->player.dir_x * cos(-(0.033 * 1.8) / 2) -
						cfg->player.dir_y * sin(-(0.033 * 1.8) / 2);
	cfg->player.dir_y = old_dir_x * sin(-(0.033 * 1.8) / 2) +
						cfg->player.dir_y * cos(-(0.033 * 1.8) / 2);
	old_plane_x = cfg->player.plane_x;
	cfg->player.plane_x = cfg->player.plane_x * cos(-(0.033 * 1.8) / 2)
						- cfg->player.plane_y * sin(-(0.033 * 1.8) / 2);
	cfg->player.plane_y = old_plane_x * sin(-(0.033 * 1.8) / 2) +
						cfg->player.plane_y * cos(-(0.033 * 1.8) / 2);
}

int		ft_move_player(int keycode, t_struct *cfg)
{
	if (keycode == 65307)
		ft_exit(cfg);
	ft_step_front_back(cfg, keycode);
	ft_step_right_left(cfg, keycode);
	if (keycode == 65361)
		ft_look_left(cfg);
	if (keycode == 65363)
		ft_look_right(cfg);
	ft_raycast(cfg);
	return (0);
}
