/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/02 18:53:10 by ameur             #+#    #+#             */
/*   Updated: 2021/03/26 20:15:57 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_count_nb_spr(t_struct *cfg)
{
	int i;
	int j;
	int res;

	i = 0;
	res = 0;
	while (i < cfg->size_map)
	{
		j = 0;
		while (cfg->map[i][j])
		{
			if (cfg->map[i][j] == '2')
				res++;
			j++;
		}
		i++;
	}
	return (res);
}

int		ft_parse_sprite(t_struct *cfg)
{
	int i;
	int j;
	int k;

	i = 0;
	k = 0;
	if (!(cfg->sprites = malloc(cfg->nb_sprite * sizeof(t_spr))))
		return (1);
	while (i < cfg->size_map)
	{
		j = 0;
		while (cfg->map[i][j])
		{
			if (cfg->map[i][j] == '2')
			{
				cfg->sprites[k].x = i + 0.5;
				cfg->sprites[k].y = j + 0.5;
				k++;
			}
			j++;
		}
		i++;
	}
	return (0);
}
