/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 11:54:02 by ameur             #+#    #+#             */
/*   Updated: 2021/04/21 16:00:03 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_init_sprite(t_struct *cfg)
{
	int i;

	i = 0;
	if (!(cfg->sprite_order = malloc(cfg->nb_sprite * sizeof(int))))
	{
		free(cfg->z_buffer);
		return (1);
	}
	if (!(cfg->sprite_dist = malloc(cfg->nb_sprite * sizeof(double))))
	{
		free(cfg->z_buffer);
		free(cfg->sprite_order);
		return (1);
	}
	while (i < cfg->nb_sprite)
	{
		cfg->sprite_order[i] = i;
		cfg->sprite_dist[i] = ((cfg->player.x - cfg->sprites[i].x) *
							(cfg->player.x - cfg->sprites[i].x) +
							(cfg->player.y - cfg->sprites[i].y) *
							(cfg->player.y - cfg->sprites[i].y));
		i++;
	}
	return (0);
}

int		ft_sort_sprite(t_struct *cfg)
{
	int		i;
	int		j;
	int		tmp;

	i = 0;
	tmp = 0;
	if (ft_init_sprite(cfg) == 1)
		return (1);
	while (i < cfg->nb_sprite)
	{
		j = -1;
		while (++j < cfg->nb_sprite - 1)
			if (cfg->sprite_dist[j] < cfg->sprite_dist[j + 1])
			{
				tmp = cfg->sprite_dist[j + 1];
				cfg->sprite_dist[j + 1] = cfg->sprite_dist[j];
				cfg->sprite_dist[j] = tmp;
				tmp = cfg->sprite_order[j + 1];
				cfg->sprite_order[j + 1] = cfg->sprite_order[j];
				cfg->sprite_order[j] = (int)tmp;
			}
		i++;
	}
	return (0);
}

void	ft_calcul_spr(t_struct *cfg, t_rspr *spr, int i)
{
	spr->sprite_x = cfg->sprites[cfg->sprite_order[i]].x - cfg->player.x;
	spr->sprite_y = cfg->sprites[cfg->sprite_order[i]].y - cfg->player.y;
	spr->inv_det = 1.0 / (cfg->player.plane_x * cfg->player.dir_y -
							cfg->player.dir_x * cfg->player.plane_y);
	spr->transform_x = spr->inv_det * (cfg->player.dir_y * spr->sprite_x -
							cfg->player.dir_x * spr->sprite_y);
	spr->transform_y = spr->inv_det * (-cfg->player.plane_y * spr->sprite_x
							+ cfg->player.plane_x * spr->sprite_y);
	spr->sprite_screen_x = (int)((cfg->rx / 2) * (1 + spr->transform_x /
								spr->transform_y));
	spr->sprite_height = abs((int)(cfg->ry / spr->transform_y));
	spr->draw_start_y = -spr->sprite_height / 2 + cfg->ry / 2;
	if (spr->draw_start_y < 0)
		spr->draw_start_y = 0;
	spr->draw_end_y = spr->sprite_height / 2 + cfg->ry / 2;
	if (spr->draw_end_y >= cfg->ry)
		spr->draw_end_y = cfg->ry - 1;
	spr->sprite_width = abs((int)(cfg->ry / (spr->transform_y)));
	spr->draw_start_x = -spr->sprite_width / 2 + spr->sprite_screen_x;
	if (spr->draw_start_x < 0)
		spr->draw_start_x = 0;
	spr->draw_end_x = spr->sprite_width / 2 + spr->sprite_screen_x;
	if (spr->draw_end_x >= cfg->rx)
		spr->draw_end_x = cfg->rx - 1;
}

void	ft_draw_sprites(t_struct *cfg, t_rspr *spr)
{
	int y;
	int d;

	spr->tex_x = (int)(256 * (spr->stripe - (-spr->sprite_width /
					2 + spr->sprite_screen_x)) * cfg->tex[4].tex_w /
						spr->sprite_width) / 256;
	if (spr->transform_y > 0 && spr->stripe >= 0 && spr->stripe <
			cfg->rx && spr->transform_y < cfg->z_buffer[spr->stripe])
	{
		y = spr->draw_start_y;
		while (y < spr->draw_end_y)
		{
			d = (y) * 256 - cfg->ry * 128 + spr->sprite_height * 128;
			spr->tex_y = ((d * cfg->tex[4].tex_h) /
							spr->sprite_height) / 256;
			if (spr->tex_y < 0)
				spr->tex_y = 0;
			if (cfg->tex[4].addr[spr->tex_y * cfg->tex[4].size_line /
					4 + spr->tex_x] != 0)
				cfg->img.addr[y * (cfg->img.l_l / 4) + spr->stripe] =
					cfg->tex[4].addr[spr->tex_y * cfg->tex[4].size_line
						/ 4 + spr->tex_x];
			y++;
		}
	}
}

void	ft_sprites(t_struct *cfg)
{
	t_rspr	spr;
	int		i;

	i = 0;
	if (ft_sort_sprite(cfg) == 1)
		return ;
	while (i < cfg->nb_sprite)
	{
		ft_calcul_spr(cfg, &spr, i);
		spr.stripe = spr.draw_start_x;
		while (spr.stripe < spr.draw_end_x)
		{
			ft_draw_sprites(cfg, &spr);
			spr.stripe++;
		}
		i++;
	}
	if (cfg->save == -1)
		mlx_put_image_to_window(cfg->img.mlx, cfg->img.mlx_win,
									cfg->img.img, 0, 0);
	free(cfg->z_buffer);
	free(cfg->sprite_order);
	free(cfg->sprite_dist);
}
