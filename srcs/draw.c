/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 11:50:24 by ameur             #+#    #+#             */
/*   Updated: 2021/04/21 14:57:54 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

t_rgb	img_get_px(t_tex tex, int x, int y)
{
	t_rgb color;

	color.r = (unsigned)*(tex.addr + (x * 4 + 2) + (tex.tex_w * y));
	color.g = (unsigned)*(tex.addr + (x * 4 + 1) + (tex.tex_w * y));
	color.b = (unsigned)*(tex.addr + (x * 4 + 0) + (tex.tex_w * y));
	return (color);
}

void	ft_draw_px(t_struct *cfg, int x, int y, int color)
{
	cfg->img.addr[y * (cfg->img.l_l / 4) + x] = color;
}

void	draw_line(t_struct *cfg, t_ray *ray, int x, int id)
{
	int		i;
	double	step;
	double	tex_pos;

	i = -1;
	ray->tex_x = (int)(ray->wall_x * (double)cfg->tex[id].tex_w);
	if (ray->side == 0 && ray->ray_dir_x > 0)
		ray->tex_x = cfg->tex[id].tex_w - ray->tex_x - 1;
	if (ray->side == 1 && ray->ray_dir_y < 0)
		ray->tex_x = cfg->tex[id].tex_w - ray->tex_x - 1;
	step = 1.0 * cfg->tex[id].tex_h / ray->line_height;
	tex_pos = (ray->draw_start - cfg->ry / 2 + ray->line_height / 2) * step;
	while (++i < ray->draw_start)
		ft_draw_px(cfg, x, i, rgb_to_int(cfg->f.r, cfg->f.g, cfg->f.b));
	while (i < ray->draw_end)
	{
		tex_pos += step;
		ft_draw_px(cfg, x, i, cfg->tex[id].addr[(int)tex_pos *
			(cfg->tex[id].size_line / 4) + ray->tex_x]);
		i++;
	}
	while (i < cfg->ry)
		ft_draw_px(cfg, x, i++, rgb_to_int(cfg->c.r, cfg->c.g, cfg->c.b));
}
