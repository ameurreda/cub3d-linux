/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_player.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/17 20:38:05 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/21 14:57:16 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	init_player(t_struct *cfg)
{
	cfg->player.x = 0.0;
	cfg->player.y = 0.0;
	cfg->player.dir_x = -0.99;
	cfg->player.dir_y = 0.0;
	cfg->player.plane_x = 0.0;
	cfg->player.plane_y = 0.66;
}

void	view_to_w(t_struct *cfg)
{
	double	old_dir_x;
	double	old_plane_x;

	old_dir_x = cfg->player.dir_x;
	cfg->player.dir_x = cfg->player.dir_x * cos(-PI * 1.5)
						- cfg->player.dir_y * sin(-PI * 1.5);
	cfg->player.dir_y = old_dir_x * sin(-PI * 1.5) +
						cfg->player.dir_y * cos(-PI * 1.5);
	old_plane_x = cfg->player.plane_x;
	cfg->player.plane_x = cfg->player.plane_x * cos(-PI * 1.5)
						- cfg->player.plane_y * sin(-PI * 1.5);
	cfg->player.plane_y = old_plane_x * sin(-PI * 1.5) +
						cfg->player.plane_y * cos(-PI * 1.5);
}

void	view_to_e(t_struct *cfg)
{
	double	old_dir_x;
	double	old_plane_x;

	old_dir_x = cfg->player.dir_x;
	cfg->player.dir_x = cfg->player.dir_x * cos(-PI / 2) -
						cfg->player.dir_y * sin(-PI / 2);
	cfg->player.dir_y = old_dir_x * sin(-PI / 2) +
						cfg->player.dir_y * cos(-PI / 2);
	old_plane_x = cfg->player.plane_x;
	cfg->player.plane_x = cfg->player.plane_x *
		cos(-PI / 2) - cfg->player.plane_y * sin(-PI / 2);
	cfg->player.plane_y = old_plane_x * sin(-PI / 2) +
						cfg->player.plane_y * cos(-PI / 2);
}

void	view_to_player(t_struct *cfg, char c)
{
	double	old_dir_x;
	double	old_plane_x;

	if (c == 'N')
	{
		cfg->player.dir_x = -0.99;
		cfg->player.dir_y = 0.0;
	}
	else if (c == 'S')
	{
		old_dir_x = cfg->player.dir_x;
		cfg->player.dir_x = cfg->player.dir_x * cos(-PI) -
							cfg->player.dir_y * sin(-PI);
		cfg->player.dir_y = old_dir_x * sin(-PI) + cfg->player.dir_y * cos(-PI);
		old_plane_x = cfg->player.plane_x;
		cfg->player.plane_x = cfg->player.plane_x * cos(-PI) -
							cfg->player.plane_y * sin(-PI);
		cfg->player.plane_y = old_plane_x * sin(-PI) +
								cfg->player.plane_y * cos(-PI);
	}
	else if (c == 'E')
		view_to_e(cfg);
	else if (c == 'W')
		view_to_w(cfg);
}

int		ft_parse_player(t_struct *cfg)
{
	int i;
	int j;

	i = -1;
	while (++i < cfg->size_map)
	{
		j = -1;
		while (cfg->map[i][++j])
		{
			if (ft_is_v_map(cfg->map[i][j]) == 0)
				return (1);
			if (is_player(cfg, i, j) == 1)
			{
				if (cfg->player.x != 0.0 || cfg->player.y != 0.0)
					return (1);
				cfg->player.x = i + 0.5;
				cfg->player.y = j + 0.5;
				view_to_player(cfg, cfg->map[i][j]);
				cfg->map[i][j] = '0';
			}
		}
	}
	if (cfg->player.x == 0.0 || cfg->player.y == 0.0)
		return (1);
	return (0);
}
