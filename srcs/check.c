/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 13:22:51 by ameur             #+#    #+#             */
/*   Updated: 2021/03/26 18:11:12 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_is_map(char *line)
{
	int i;

	i = 0;
	while ((line[i] == ' ' || line[i] == '	') && line[i])
		i++;
	if (line[i] == '1')
		return (0);
	return (1);
}

int		ft_check_path(t_struct *cfg)
{
	if (cfg->tex[0].path == NULL)
		return (1);
	if (cfg->tex[1].path == NULL)
		return (1);
	if (cfg->tex[2].path == NULL)
		return (1);
	if (cfg->tex[3].path == NULL)
		return (1);
	if (cfg->tex[4].path == NULL)
		return (1);
	return (0);
}

void	ft_strcpy(char *src, char *dest, int i)
{
	int j;

	j = 0;
	while (src[i] != ' ' && src[i])
	{
		dest[j] = src[i];
		i++;
		j++;
	}
	dest[j] = '\0';
}

int		ft_check_parse(t_struct *cfg)
{
	if (cfg->rx == -1 || cfg->ry == -1 || cfg->f.r == -1 ||
		cfg->f.g == -1 || cfg->f.b == -1 || cfg->c.r == -1 ||
		cfg->c.g == -1 || cfg->c.b == -1 || ft_check_path(cfg) == 1)
	{
		cfg->res = 5;
		return (1);
	}
	return (0);
}

int		ft_check_xpm(t_struct *cfg)
{
	int fd;
	int i;
	int j;

	i = 0;
	while (i < 5)
	{
		fd = open(cfg->tex[i].path, O_RDONLY);
		if (fd < 0)
			return (1);
		j = ft_len(cfg->tex[i].path) - 1;
		if (cfg->tex[i].path[j] == ' ')
			while (cfg->tex[i].path[j] == ' ' || cfg->tex[i].path[j])
				i--;
		if (cfg->tex[i].path[j] != 'm')
			return (1);
		i++;
	}
	return (0);
}
