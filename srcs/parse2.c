/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/20 15:46:31 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/15 08:11:08 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int		ft_parse_r_bis(t_struct *cfg, char *line, int i)
{
	while (line[i])
	{
		if (ft_is_valid(line[i], cfg) == 0)
			return (1);
		if (line[i] >= 48 && line[i] <= 57)
		{
			if (cfg->rx == -1)
			{
				cfg->rx = ft_atoi(&line[i]);
				while (line[i] >= 48 && line[i] <= 57)
					i++;
			}
			else if (cfg->ry == -1)
			{
				cfg->ry = ft_atoi(&line[i]);
				while (line[i] >= 48 && line[i] <= 57)
					i++;
			}
		}
		if (line[i] != '\0')
			i++;
	}
	return (0);
}

int		ft_parse_r(char *line, t_struct *cfg, int i)
{
	if (ft_parse_r_bis(cfg, line, i) == 1)
	{
		cfg->res = 12;
		return (1);
	}
	if (cfg->rx <= 0 || cfg->ry <= 0)
	{
		cfg->res = 2;
		return (1);
	}
	return (0);
}

int		ft_path_to_cfg(t_struct *cfg, int mode, char *line, int i)
{
	if (mode == 2)
	{
		if (!(cfg->tex[1].path = malloc(ft_to_malloc(line, i) + 1)))
			return (cfg->res = 1);
		ft_strcpy(line, cfg->tex[1].path, i);
	}
	if (mode == 3)
	{
		if (!(cfg->tex[2].path = malloc(ft_to_malloc(line, i) + 1)))
			return (cfg->res = 1);
		ft_strcpy(line, cfg->tex[2].path, i);
	}
	if (mode == 4)
	{
		if (!(cfg->tex[3].path = malloc(ft_to_malloc(line, i) + 1)))
			return (cfg->res = 1);
		ft_strcpy(line, cfg->tex[3].path, i);
	}
	if (mode == 5)
	{
		if (!(cfg->tex[4].path = malloc(ft_to_malloc(line, i) + 1)))
			return (cfg->res = 1);
		ft_strcpy(line, cfg->tex[4].path, i);
	}
	return (0);
}

int		ft_parse_path(char *line, t_struct *cfg, int mode, int i)
{
	int b;

	b = 0;
	while (line[i])
	{
		if (line[i] == '.' && line[i + 1] == '/')
		{
			if (mode == 1)
			{
				if (!(cfg->tex[0].path = malloc(ft_to_malloc(line, i) + 1)))
					return (cfg->res = 1);
				ft_strcpy(line, cfg->tex[0].path, i);
			}
			if (ft_path_to_cfg(cfg, mode, line, i) == 1)
				return (1);
			b = 1;
		}
		if (line[i] != '.' && line[i] != ' ' && b == 0)
			return (1);
		i++;
	}
	return (0);
}

int		ft_check_rgb(t_struct *cfg, int mode)
{
	if (mode == 1)
	{
		if (cfg->f.r > 255 || cfg->f.r < 0)
			return (1);
		if (cfg->f.g > 255 || cfg->f.g < 0)
			return (1);
		if (cfg->f.b > 255 || cfg->f.b < 0)
			return (1);
	}
	else if (mode == 2)
	{
		if (cfg->c.r > 255 || cfg->c.r < 0)
			return (1);
		if (cfg->c.g > 255 || cfg->c.g < 0)
			return (1);
		if (cfg->c.b > 255 || cfg->c.b < 0)
			return (1);
	}
	return (0);
}
