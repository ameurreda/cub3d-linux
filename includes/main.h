/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/20 12:27:37 by ameurreda         #+#    #+#             */
/*   Updated: 2021/04/19 16:38:46 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <math.h>

# include "mlx.h"

# define PI 3.14159265

/*
**l_l = line_lenght
*/
typedef struct	s_data {
	void		*mlx;
	void		*mlx_win;
	void		*img;
	char		*relative_path;
	int			*addr;
	int			bits_per_pixel;
	int			l_l;
	int			endian;
	int			cx;
	int			cy;
	int			img_w;
	int			img_h;
}				t_data;

typedef struct	s_rgb {
	int			r;
	int			g;
	int			b;
}				t_rgb;

typedef struct	s_player {
	double		x;
	double		y;
	double		dir_x;
	double		dir_y;
	double		plane_x;
	double		plane_y;
}				t_player;

typedef struct	s_tex
{
	char		*path;
	int			*addr;
	void		*tex;
	int			tex_h;
	int			tex_w;
	int			bpp;
	int			size_line;
	int			endian;
}				t_tex;

typedef struct	s_spr
{
	double		x;
	double		y;
}				t_spr;

typedef struct	s_list
{
	char			*content;
	struct s_list	*next;
}				t_lst;

/*
**tex:
**NO = 0;
**SO = 1;
**WE = 2;
**EA = 3;
**srp = 4;
*/
typedef struct	s_struct {
	t_lst		*lst;
	t_tex		tex[5];
	t_rgb		f;
	t_rgb		c;
	char		**map;
	int			size_map;
	t_player	player;
	t_data		img;
	int			rx;
	int			ry;
	int			nb_sprite;
	t_spr		*sprites;
	double		*z_buffer;
	int			*sprite_order;
	double		*sprite_dist;
	int			res;
	int			save;
	int			is_map;
	int			fd;
}				t_struct;

typedef struct	s_rspr
{
	double		sprite_x;
	double		sprite_y;
	double		inv_det;
	double		transform_x;
	double		transform_y;
	int			sprite_screen_x;
	int			sprite_height;
	int			draw_start_y;
	int			draw_end_y;
	int			sprite_width;
	int			draw_start_x;
	int			draw_end_x;
	int			stripe;
	int			tex_x;
	int			tex_y;
}				t_rspr;

typedef struct	s_ray
{
	double		camera_x;
	double		ray_dir_x;
	double		ray_dir_y;
	int			map_x;
	int			map_y;
	double		side_dist_x;
	double		side_dist_y;
	double		delta_dist_x;
	double		delta_dist_y;
	double		perp_wall_dist;
	int			step_x;
	int			step_y;
	int			hit;
	int			side;
	int			line_height;
	int			draw_start;
	int			draw_end;
	double		wall_x;
	int			tex_x;
	int			tex_y;
}				t_ray;

void			ft_strcpy(char *src, char *dest, int i);
int				ft_to_malloc(char *line, int i);
int				ft_atoi(const char *str);
int				get_next_line(int fd, char **line);
int				ft_parse(int fd, t_struct *config);
int				ft_parse_line(char *line, t_struct *config);
int				ft_parse_r(char *line, t_struct *config, int i);
int				ft_parse_path(char *line, t_struct *config, int mode, int i);
int				ft_parse_f(char *line, t_struct *config, int i);
int				ft_parse_c(char *line, t_struct *config, int i);
int				ft_check_rgb(t_struct *config, int mode);
int				ft_parse_map(t_struct *config, char *line, int fd);
int				ft_check_map(t_struct *config);
t_lst			*ft_lstnew(void *content);
int				ft_lstsize(t_lst *lst);
void			ft_lstdelone(t_lst *lst, void (*del)(void*));
void			ft_lstclear(t_struct *cfg);
void			ft_lstiter(t_lst *lst, int (printf)(const char *, ...));
t_lst			*ft_lstmap(t_lst *lst, void *(*f)(void *), void (*del)(void *));
void			ft_lstadd_back(t_lst **alst, t_lst *new);
void			del(char *content);
void			ft_display_minimap(t_struct *config);
int				rgb_to_int(int r, int g, int b);
void			ft_display_player(t_struct *config);
int				is_player(t_struct *config, int i, int j);
int				ft_parse_player(t_struct *config);
int				ft_is_escape(int keycode, t_data *img);
int				ft_move_player(int keycode, t_struct *config);
void			init_player(t_struct *config);
void			my_mlx_pixel_put(t_data *data, int x, int y, int color);
int				ft_raycast(t_struct *config);
int				ft_parse_sprite(t_struct *config);
int				ft_count_nb_spr(t_struct *config);
void			ft_sprites(t_struct *config);
void			test_spr(t_struct *config);
int				ft_path(t_struct *config);
t_rgb			img_get_px(t_tex tex, int x, int y);
void			ft_draw_px(t_struct *cfg, int x, int y, int color);
void			draw_line(t_struct *cfg, t_ray *ray, int x, int id);
int				ft_lst_to_map(t_struct *cfg);
int				ft_check_line(t_struct *cfg, int i);
int				ft_len(char *str);
int				ft_longest_line(t_lst *lst);
int				ft_is_map(char *line);
int				ft_check_path(t_struct *cfg);
int				ft_str_cmp(char *s1, char *s2);
void			ft_save_bmp(t_struct *cfg);
int				ft_exit(t_struct *cfg);
char			*ft_strdup(const char *s1, int destsize);
int				ft_mlx(t_struct *cfg);
void			ft_init_struct(t_struct *cfg);
int				ft_check_parse(t_struct *cfg);
void			ft_print_error(t_struct *cfg);
int				ft_check_xpm(t_struct *cfg);
int				ft_is_cub(char **argv);
int				ft_parse_line_t(char *line, t_struct *cfg, int i);
char			*ft_dup(char *s);
int				ft_line_to_lst(t_struct *cfg, char *line);
void			ft_init_struct(t_struct *cfg);
int				ft_is_info(char *s, t_struct *cfg, int i);
int				ft_is_valid(char c, t_struct *cfg);
int				ft_count_i(char *line, int i);
int				ft_is_v(char c, int v);
int				ft_parse_c_t(t_struct *cfg, char *line, int i);
int				ft_is_v_map(char c);
void			ft_cpy(char *src, char *dest, int i);

#endif
