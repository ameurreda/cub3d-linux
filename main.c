/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/20 12:27:32 by ameurreda         #+#    #+#             */
/*   Updated: 2021/03/26 21:04:09 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/main.h"

int		ft_path(t_struct *cfg)
{
	if (ft_check_xpm(cfg) == 1)
		return (1);
	cfg->tex[0].tex = mlx_xpm_file_to_image(cfg->img.mlx,
		cfg->tex[0].path, &cfg->tex[0].tex_w, &cfg->tex[0].tex_h);
	cfg->tex[0].addr = (int*)mlx_get_data_addr(cfg->tex[0].tex,
		&cfg->tex[0].bpp, &cfg->tex[0].size_line, &cfg->tex[0].endian);
	cfg->tex[1].tex = mlx_xpm_file_to_image(cfg->img.mlx,
			cfg->tex[1].path, &cfg->tex[1].tex_w, &cfg->tex[1].tex_h);
	cfg->tex[1].addr = (int*)mlx_get_data_addr(cfg->tex[1].tex,
		&cfg->tex[1].bpp, &cfg->tex[1].size_line, &cfg->tex[1].endian);
	cfg->tex[2].tex = mlx_xpm_file_to_image(cfg->img.mlx,
			cfg->tex[2].path, &cfg->tex[2].tex_w, &cfg->tex[2].tex_h);
	cfg->tex[2].addr = (int*)mlx_get_data_addr(cfg->tex[2].tex,
		&cfg->tex[2].bpp, &cfg->tex[2].size_line, &cfg->tex[2].endian);
	cfg->tex[3].tex = mlx_xpm_file_to_image(cfg->img.mlx,
			cfg->tex[3].path, &cfg->tex[3].tex_w, &cfg->tex[3].tex_h);
	cfg->tex[3].addr = (int*)mlx_get_data_addr(cfg->tex[3].tex,
		&cfg->tex[3].bpp, &cfg->tex[3].size_line, &cfg->tex[3].endian);
	cfg->tex[4].tex = mlx_xpm_file_to_image(cfg->img.mlx,
			cfg->tex[4].path, &cfg->tex[4].tex_w, &cfg->tex[4].tex_h);
	cfg->tex[4].addr = (int*)mlx_get_data_addr(cfg->tex[4].tex,
		&cfg->tex[4].bpp, &cfg->tex[4].size_line, &cfg->tex[4].endian);
	return (0);
}

void	ft_size_screen(t_struct *cfg)
{
	int	screen_size_x;
	int	screen_size_y;

	screen_size_x = 0;
	screen_size_y = 0;
	mlx_get_screen_size(cfg->img.mlx, &screen_size_x, &screen_size_y);
	if (cfg->rx > screen_size_x)
		cfg->rx = screen_size_x;
	if (cfg->ry > screen_size_y)
		cfg->ry = screen_size_y;
}

int		ft_parse_error(t_struct *cfg, char **argv)
{
	cfg->fd = open(argv[1], O_RDONLY);
	if (cfg->fd < 1 || ft_is_cub(argv) == 0)
	{
		printf("Error.\nThe file is not valid.\n");
		return (1);
	}
	if (ft_parse(cfg->fd, cfg) == 1)
	{
		ft_print_error(cfg);
		ft_exit(cfg);
		return (1);
	}
	if (ft_mlx(cfg) == 1)
	{
		ft_print_error(cfg);
		ft_exit(cfg);
		return (1);
	}
	return (0);
}

int		ft_mlx(t_struct *cfg)
{
	cfg->img.mlx = mlx_init();
	ft_size_screen(cfg);
	cfg->img.img = mlx_new_image(cfg->img.mlx, cfg->rx, cfg->ry);
	cfg->img.addr = (int*)mlx_get_data_addr(cfg->img.img,
		&cfg->img.bits_per_pixel, &cfg->img.l_l, &cfg->img.endian);
	if (ft_path(cfg) == 1)
	{
		cfg->res = 6;
		return (1);
	}
	if (cfg->save == -1)
		cfg->img.mlx_win = mlx_new_window(cfg->img.mlx, cfg->rx,
			cfg->ry, "Cub3D");
	if (ft_raycast(cfg) == 1)
		return (1);
	if (cfg->save == -1)
	{
		mlx_hook(cfg->img.mlx_win, 33, 1L << 17, ft_exit, cfg);
		mlx_hook(cfg->img.mlx_win, 2, 1L << 0, ft_move_player, cfg);
	}
	mlx_loop(cfg->img.mlx);
	if (cfg->save == 1)
		ft_save_bmp(cfg);
	return (0);
}

int		main(int argc, char **argv)
{
	t_struct	cfg;

	init_player(&cfg);
	ft_init_struct(&cfg);
	if (argc == 2)
	{
		if (ft_parse_error(&cfg, argv) == 1)
			return (0);
	}
	else if (argc == 3 && ft_str_cmp("--save", argv[2]) == 0)
	{
		cfg.save = 1;
		if (ft_parse_error(&cfg, argv) == 1)
			return (0);
	}
	else
		printf("Error.\n");
	return (0);
}
