SRCS		= main.c \
			gnl/get_next_line.c \
			gnl/get_next_line_utils.c\
			srcs/parse1.c\
			srcs/parse2.c\
			srcs/parse3.c\
			srcs/parse4.c\
			srcs/lst.c\
			srcs/lst2.c\
			srcs/display_map.c\
			srcs/move_player.c\
			srcs/parse_map.c\
			srcs/parse_map2.c\
			srcs/parse_player.c\
			srcs/raycast.c\
			srcs/sprite.c\
			srcs/sprite2.c\
			srcs/draw.c\
			srcs/check.c\
			srcs/parse_f_c.c\
			srcs/bmp.c\
			srcs/exit.c\
			srcs/ft_atoi.c\
			srcs/ft_to_malloc.c
			

CC		= gcc
CFLAGS		= -Wall -Wextra -Werror -lmlx -lm -lbsd -lX11 -lXext -g#-fsanitize=address
DIR_INC		= -I./minilibx-linux/ -L./minilibx-linux/

NAME		= cub3D

OBJS		= ${SRCS:.c=.o}

all:		$(NAME)

.c.o:
			${CC} ${CFLAGS} -c ${DIR_INC} $< -o ${<:.c=.o}

$(NAME):	$(OBJS)
			${CC} -o ${NAME} ${DIR_INC} ${OBJS} ${CFLAGS}

clean:
			rm -f srcs/*.o main.o gnl/get_next_line.o gnl/get_next_line_utils.o includes/ft_atoi.o includes/ft_to_malloc.o

fclean:		clean
			${RM} ${NAME}


re:		clean all
