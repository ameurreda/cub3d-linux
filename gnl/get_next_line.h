/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 13:20:13 by rameur            #+#    #+#             */
/*   Updated: 2021/03/25 23:06:55 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdio.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>

# define BUFFER_SIZE 1

int		get_next_line(int fd, char **line);
char	*ft_strjoin(const char *s1, const char *s2, int l, int mode);
int		ft_strlen(char *str);
size_t	ft_strlcpy(char *dest, char *src, size_t destsize);
char	*ft_substr(char *s, int start, int len);
char	*ft_strdup(const char *s1, int destsize);
void	ft_putstr_fd(char *str, int fd);
void	ft_free(void *str);

#endif
