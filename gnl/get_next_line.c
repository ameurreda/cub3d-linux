/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 14:52:21 by rameur            #+#    #+#             */
/*   Updated: 2021/03/23 22:15:05 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		ft_strlen_check(char *str, int mode)
{
	int		i;

	i = 0;
	while (str[i] != '\n' && str[i] != '\0')
		i++;
	if (mode == 1)
	{
		if (str[i] == '\n')
			return (1);
		else if (str[i] == '\0')
			return (2);
		else
			return (0);
	}
	else if (mode == 2)
	{
		if (str[i] == '\n')
			return (i);
		else
			return (-1);
	}
	else if (mode == 3)
		return (i);
	return (-1);
}

void	ft_free(void *str)
{
	free(str);
	str = NULL;
}

int		gnl(char **line, char **str, int ret)
{
	if (ft_strlen_check(*str, 1) == 1)
	{
		*line = ft_strdup(*str, ft_strlen_check(*str, 3));
		*str = ft_substr(*str, ft_strlen_check(*str, 3) + 1, ft_strlen(*str));
		return (1);
	}
	else if (ft_strlen_check(*str, 1) == 2 && ret == 0)
	{
		*line = ft_strdup(*str, ft_strlen_check(*str, 3));
		free(*str);
		*str = NULL;
		return (0);
	}
	return (-1);
}

int		get_next_line(int fd, char **line)
{
	static char	*str;
	char		*buff;
	int			ret;

	ret = 1;
	if (!(buff = malloc((BUFFER_SIZE + 1) * sizeof(char))))
		return (-1);
	buff[BUFFER_SIZE] = '\0';
	if (fd < 0 || read(fd, buff, 0) == -1 || !(line) || BUFFER_SIZE <= 0)
		return (-1);
	if (!str)
		str = ft_strdup("", 1);
	while (ft_strlen_check(str, 1) != 1 && ret > 0)
	{
		ret = read(fd, buff, BUFFER_SIZE);
		buff[ret] = '\0';
		str = ft_strjoin(str, buff, ft_strlen(buff), 2);
	}
	ft_free(buff);
	return (gnl(line, &str, ret));
}
