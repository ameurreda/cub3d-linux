/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ameur <ameur@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 14:43:23 by rameur            #+#    #+#             */
/*   Updated: 2021/03/23 22:20:50 by ameur            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		ft_strlen(char *str)
{
	size_t i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strjoin(const char *s1, const char *s2, int l, int mode)
{
	int		i;
	int		j;
	int		to_m;
	char	*res;

	i = 0;
	j = 0;
	to_m = 0;
	while (s2[to_m] && to_m < l)
		to_m++;
	if (!(res = malloc((ft_strlen((char*)s1) + to_m + 1) * sizeof(char))))
	{
		if (mode == 2)
			free((void*)s1);
		return (NULL);
	}
	while (s1[i])
		res[j++] = s1[i++];
	i = 0;
	while (s2[i] && i < to_m)
		res[j++] = s2[i++];
	res[j] = '\0';
	if (mode == 2)
		free((void*)s1);
	return (res);
}

char	*ft_substr(char *s, int start, int len)
{
	char	*res;
	int		i;

	i = 0;
	if (!(s) || start >= len)
	{
		free(s);
		s = NULL;
		return (NULL);
	}
	if (!(res = malloc(sizeof(char) * len + 1)))
		return (NULL);
	if (ft_strlen(s) > start)
	{
		while (s[start] && i < len)
		{
			res[i] = s[start];
			i++;
			start++;
		}
	}
	res[i] = '\0';
	free(s);
	return (res);
}

char	*ft_strdup(const char *s1, int destsize)
{
	int		i;
	char	*res;

	i = 0;
	while (s1[i] && i < destsize)
		i++;
	if (!(res = malloc((i + 1) * sizeof(char))))
		return (NULL);
	i = 0;
	while (s1[i] && i < destsize)
	{
		res[i] = s1[i];
		i++;
	}
	res[i] = '\0';
	return (res);
}
